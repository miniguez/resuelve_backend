# resuelve_backend

## comando utilizado para crear el proyecto 
```
$ mix phx.new app --no-webpack --no-ecto --no-html --no-gettext --no-dashboard
```

## Correr el proyecto
```
$ mix deps.get
$ mix phx.server
```

## Probar con curl 
Generar token
```
$iex -S mix phx.server
iex> AppWeb.API.Auth.generate_token("user_api")
"xxxxxxxxxx"
```
Desde otra terminal
```
curl --request POST \
  --url http://localhost:4000/api/ \
  --header 'Authorization: Bearer SFMyNTY.g2gDbQAAAAh1c2VyX2FwaW4GAMub9E97AWIAAVGA.lFHAFzzvYoW0p9Dl8ejJNUHIabD4vLMn0gTOaDifBoY' \
  --header 'Content-Type: application/json' \
  --data '{
   "jugadores" : [        
      {  
         "nombre":"Luis",
         "nivel":"Cuauh",
         "goles":19,
         "sueldo":50000,
         "bono":10000,
         "sueldo_completo":null,
         "equipo":"rojo"

      }]
}'
```