defmodule App.Calculos do
  @moduledoc """
  Módulo con cálculos de sueldos.
  """

  defp goles_anotados_equipos(data) do
    Enum.group_by(data["jugadores"], fn %{"equipo" => x} -> x end)
    |> Enum.map(fn {equipo, y} ->
      {equipo, Enum.reduce(y, 0, fn x, acc -> x["goles"] + acc end)}
    end)
    |> Enum.into(%{})
  end

  defp porcentaje_equipo(data, minimo_requerido_equipo) do
    data
    |> goles_anotados_equipos()
    |> Enum.map(fn {k, v} -> {k, v / minimo_requerido_equipo} end)
    |> Enum.into(%{})
  end

  @doc """
  Sueldo: calcula los sueldos de los integrantes de equipos 
  """
  def sueldo(data, parametros) do
    equipos = porcentaje_equipo(data, parametros.minimo_requerido_equipo)
    %{jugadores: Enum.map(data["jugadores"], fn x -> calcular(x, equipos, parametros) end)}
  end

  defp calcular(jugador, equipos, parametros) do
    goles_minimos = parametros.niveles[String.to_atom(jugador["nivel"])]
    porcentaje_equipo = equipos[jugador["equipo"]]

    sueldo_completo =
      calcular_sueldo_completo(jugador, goles_minimos, porcentaje_equipo)
      |> Decimal.from_float()
      |> Decimal.round(2)

    %{
      nombre: jugador["nombre"],
      goles_minimos: goles_minimos,
      goles: jugador["goles"],
      sueldo: jugador["sueldo"],
      bono: jugador["bono"],
      sueldo_completo: sueldo_completo,
      equipo: jugador["equipo"]
    }
  end

  defp calcular_sueldo_completo(jugador, goles_minimos, porcentaje_equipo) do
    porcentaje_individual =
      if jugador["goles"] >= goles_minimos do
        1
      else
        jugador["goles"] / goles_minimos
      end

    porcentaje = (porcentaje_individual + porcentaje_equipo) / 2
    jugador["sueldo"] + jugador["bono"] * porcentaje
  end
end
