defmodule AppWeb.API.Auth do
  @moduledoc """
  Módulo para verificar token bearer
  """

  import Plug.Conn
  import Phoenix.Controller

  def init(opts), do: opts

  def call(conn, _opts) do
    conn
    |> get_token()
    |> verify_token()
    |> case do
      {:ok, user_id} -> assign(conn, :current_user, user_id)
      _unauthorized -> assign(conn, :current_user, nil)
    end
  end

  @doc """
  Función para auntenticar
  A function plug that ensures that `:current_user` value is present.  
  """
  def authenticate_api_user(conn, _opts) do
    if Map.get(conn.assigns, :current_user) do
      conn
    else
      conn
      |> put_status(:unauthorized)
      |> put_view(AppWeb.ErrorView)
      |> render(:"401")
      # Stop any downstream transformations.
      |> halt()
    end
  end

  @doc """
  Función para generar token
  ## Examples
      iex> AppWeb.API.Auth.generate_token("user_api")
      "SFMyNTY.g2gDbQAAAAh1c2VyX2FwaW4GAJRUlk97AWIAAVGA.J8HswW8TIjUD6T1Qn-CY9uDU5MT6TdokSlgJHfYhAyk"        
  """
  def generate_token(user_id) do
    Phoenix.Token.sign(
      AppWeb.Endpoint,
      inspect(__MODULE__),
      user_id
    )
  end

  @doc """
  Verificar token de usuario

  ## Examples

      iex> AppWeb.API.Auth.verify_token("good-token")
      {:ok, 1}

      iex> AppWeb.API.Auth.verify_token("bad-token")
      {:error, :invalid}
  """
  def verify_token(token) do
    one_month = 30 * 24 * 60 * 60

    Phoenix.Token.verify(
      AppWeb.Endpoint,
      inspect(__MODULE__),
      token,
      max_age: one_month
    )
  end

  def get_token(conn) do
    case get_req_header(conn, "authorization") do
      ["Bearer " <> token] -> token
      _ -> nil
    end
  end
end
