defmodule AppWeb.Router do
  use AppWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
    plug AppWeb.API.Auth
  end

  scope "/api", AppWeb do
    pipe_through [:api, :authenticate_api_user]
    post "/", SueldoController, :calcular
  end
end
