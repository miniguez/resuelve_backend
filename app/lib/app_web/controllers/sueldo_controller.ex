defmodule AppWeb.SueldoController do
  use AppWeb, :controller

  alias App.Calculos

  def calcular(conn, params) do
    parametros = %{
      minimo_requerido_equipo: 50,
      niveles: %{
        A: 5,
        B: 10,
        C: 15,
        Cuauh: 20
      }
    }

    json(conn, Calculos.sueldo(params, parametros))
  end
end
