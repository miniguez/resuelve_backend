defmodule App.CalculosTest do
  use ExUnit.Case
  alias App.Calculos

  @parametros %{minimo_requerido_equipo: 50, niveles: %{A: 5, B: 10, C: 15, Cuauh: 20}}
  @equipos %{
    "jugadores" => [
      %{
        "bono" => 10000,
        "equipo" => "rojo",
        "goles" => 19,
        "nivel" => "Cuauh",
        "nombre" => "Luis",
        "sueldo" => 50000,
        "sueldo_completo" => nil
      }
    ]
  }
  @sueldos_calculados %{
    jugadores: [
      %{
        bono: 10000,
        equipo: "rojo",
        goles: 19,
        goles_minimos: 20,
        nombre: "Luis",
        sueldo: 50000,
        sueldo_completo: Decimal.from_float(56650.00) |> Decimal.round(2)
      }
    ]
  }

  test "calcular " do
    assert Calculos.sueldo(@equipos, @parametros) == @sueldos_calculados
  end
end
