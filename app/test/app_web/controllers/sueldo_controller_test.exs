defmodule AppWeb.SueldoControllerTest do
  use AppWeb.ConnCase

  @equipos %{
    "jugadores" => [
      %{
        "bono" => 10000,
        "equipo" => "rojo",
        "goles" => 19,
        "nivel" => "Cuauh",
        "nombre" => "Luis",
        "sueldo" => 50000,
        "sueldo_completo" => nil
      }
    ]
  }
  @sueldos_calculados %{
    "jugadores" => [
      %{
        "bono" => 10000,
        "equipo" => "rojo",
        "goles" => 19,
        "goles_minimos" => 20,
        "nombre" => "Luis",
        "sueldo" => 50000,
        "sueldo_completo" => "56650.00"
      }
    ]
  }

  describe "calcular" do
    test "return json map", %{conn: conn} do
      token = AppWeb.API.Auth.generate_token("user_api")

      conn =
        conn
        |> put_req_header(
          "authorization",
          "Bearer " <> token
        )
        |> post(Routes.sueldo_path(conn, :calcular), @equipos)

      assert @sueldos_calculados == Jason.decode!(conn.resp_body)
    end
  end
end
